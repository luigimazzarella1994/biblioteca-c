/*
Author: Mazzarella Luigi N86001414
*/

#ifndef ABR_h
#define ABR_h


//struttura nodo
typedef struct nodo{
	char key[100];
	int disponibile;
	char matricola[100];
	struct nodo *left;
	struct nodo *right;
}nodo;

/*Richiamo delle funzioni*/
//inserisce un noodo nell'albero
nodo *Insert(nodo *, char *);
//ricerca nell'albero
void search(nodo *, char *, int, char*, int*);
//stampa in ordine l'albero
void stampaInorder(nodo *);
//Crea un nuovo nodo nell'albero
nodo *CreaNodo(char*); 
//verifica se il libro esiste
int libroEsiste(nodo *,char*,int );
//dealloca l'albero
void dealloca(nodo *);
//conta il numero di nodi
int conteggio(nodo *);
//verifica se l'albero � vuoto
int isEmpty(nodo*);

#endif
