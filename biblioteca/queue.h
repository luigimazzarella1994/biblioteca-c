/*
Author: Mazzarella Luigi N86001414
*/

#ifndef queue_h
#define queue_h

typedef struct studente {
	
  char matricola[100];
  //request_type tipo;
  int tipo;
  char libro[100];
  int verify;
  struct studente *next;
  
} studente;

//crea nuovo nodo
studente* creaNodo(char*, int , char*,int);
//inizializza coda
studente* init(studente*,char*, int, char*,int);
//cancella
studente* canc(studente*);
//stampa
void stamp(studente*);
//verifica se la coda è vuota
int vuoto(studente *);
//cancella un elemento nella lista
studente* cancElem(studente* , studente*);


#endif
