#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "queue.h"

//inizializza coda
studente* init(studente* top,char* matricola, int request, char* titolo,int stato) {
	if (top== NULL){

		top = creaNodo(matricola,request,titolo,stato);
	}
	else {
		studente* tmp = top;
		while(tmp->next != NULL){
			tmp= tmp->next;
		}
		tmp->next = creaNodo(matricola,request,titolo,stato);
	}

	return top;
}

//crea nodo
studente* creaNodo(char*matricola, int request, char* titolo,int stato){
	studente *tmp;
	tmp=(studente*) malloc(sizeof(studente));
	strcpy(tmp->matricola, matricola);
	tmp->tipo = request;
	strcpy(tmp->libro, titolo);
	tmp->verify = stato;
	tmp->next = NULL;
	return tmp;
}

//cancella
studente* canc(studente* top){
	if(top!=NULL){
	
			studente* tmp= top;
			top= top->next;
			free(tmp);
		
	}
	return top;
}



studente* cancElem(studente* L, studente*elem) {
    if (L != NULL) {
        if (L == elem) {
            studente* tmp = L->next;
            free(L);
            return tmp;
        }
        L->next = cancElem(L->next, elem);
    }
    return L;
}



//stampa
void stamp(studente* top){
	while ( top != NULL){
		char richiesta[100];
		char stato[100];
		if(top->tipo == 0) {
			strcpy(richiesta,"Prestito");
		}
		else {
			strcpy(richiesta,"Restituzione");
		}
		if(top->verify	== 0) {
			strcpy(stato,"Richiesta non elaborata");
		}
		else {
			strcpy(stato,"Sospeso");
		}
		printf("\n\nmatricola: %s \nrichiesta: %s\ntitolo: %s \nstato della richiesta: %s\n", top->matricola,  richiesta, top->libro,stato);
		top = top->next;
	}
}

int vuoto(studente *top){
	if( top == NULL) return 1;
	return 0;
}

