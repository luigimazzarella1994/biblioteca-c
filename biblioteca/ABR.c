/*
AUTHOR: Mazzarella Luigi N86001414

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "ABR.h"

//inserisci Un elemento nell'albero
nodo *Insert(nodo *root, char *key){	
	if(root == NULL)
	{
		root = CreaNodo(key);
	}
	else{
	
			int index = 0;
			for(index; index< strlen(root->key);index++){
				if(root->key[index] != key[index]){
					break;
				}
			}
			if( root->key[index] > key[index]) {
				root->left = Insert(root->left,key);
			} 
			else  {
				root->right = Insert(root->right, key);
        	}
			
		
	}
	return root;
}


//Crea un nodo
nodo *CreaNodo(char* key){
	nodo *tmp;
	tmp=(nodo*)malloc(sizeof(nodo));
	strcpy(tmp->key,key);
	tmp->disponibile = 1;
	strcpy(tmp->matricola,"Nessuno");
	tmp->left=NULL;
	tmp->right=NULL;
	return tmp;
}



//Funzione che verifica se l'albero e' vuoto
int isEmpty(nodo *root){
	if (root==NULL)
		return 1;
	else
		return 0;
}



//stampa in order
void stampaInorder(nodo *root){
	
    if(root!=NULL){
        stampaInorder(root->left);
    	printf("\nTITOLO: %s Disponibilitą : %d\n Matricola assegnata: %s\n ", root->key, root->disponibile,root->matricola);
        stampaInorder(root->right);
    }
}



//ricerca di un libro nell'albero
void search(nodo *root, char *key, int richiesta, char* matricola,int *disp){
	
	  if(root!=NULL){
	  	
        	search(root->left, key, richiesta,matricola,disp);
        	
        	if(strcmp(root->key, key) == 0){
        		if( richiesta == 0) {
        			if(root->disponibile == 1) {
        				strcpy(root->matricola,matricola);
        				root->disponibile =0;
        				*disp = 0;
        				int elm = *disp;
        				if(elm == 1) {
        					*disp = 0;
        				}
        			}
        		
        			else {
        				//printf("\nLibro non disponibile (%s)",root->key);
        				*disp = 1;
        			}
        		}
        		else {
        			if(strcmp(root->matricola, matricola) == 0){
        				printf("\nSto restituendo il libro");
        				root->disponibile = 1;
						strcpy(root->matricola,"Nessuno");
        				*disp = 0;
        			}
					else printf("\nMatricola n. %s non trovata in elenco",root->matricola);
        		}
        	
        	}
       		 search(root->right,key,richiesta,matricola,disp);
        	
    	}
	
}


//verifica se il libro esiste nell'abero
int libroEsiste(nodo *root,char *titolo, int i){
    if(root!=NULL){
        i=libroEsiste(root->left,titolo,i);
    	if(strcmp(root->key,titolo) == 0){
			i = 1;;
		}
        i=libroEsiste(root->right,titolo,i);
    }

	return i;
}




//Deallocazione abr
void dealloca(nodo *abr){
	if (isEmpty(abr)){
		free(abr);
	}else{
		dealloca(abr->left);
		dealloca(abr->right);
		free(abr);
	}
	return;
}

//Funzione che conta il numero di nodi in un abr
int conteggio(nodo *root){
	if (root!=NULL)
		return (conteggio(root->left) + conteggio(root->right)) + 1;
	return 0;
}


