/******************************************************************************
Author Luigi Mazzarella
Date 05.05.2020
C-language

*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include "ABR.h"
#include "queue.h"




int main() {
	
	
	//Abr per la biblioteca	
	nodo *biblio = NULL;
	//coda di richieste
	studente *queue = NULL;
	//coda di richieste sospese
	studente *sospesi = NULL;
	
	
	//Lista di libri per inizializzare la biblioteca
	char *libri[15] = {"Il signore degli anelli\n",
	    "Harry Potter\n",
    	"La bibbia\n",
    	"Piccole donne\n",
    	"Guarnizione\n",
    	"Il nome della rosa\n",
    	"Via col vento\n",
    	"Odissea\n",
    	"Cuore\n",
    	"Malavoglia\n",
    	"Siddharta\n",
    	"Lolita\n",
    	"Amabili resti\n",
    	"Sulla strada\n",
    	"Possession\n"};
	
	//variabile per ciclo for
	int i=0;
	//creazione albero
	for ( i = 0; i< 15; i++){
		biblio = Insert(biblio,libri[i]);
	}
	
	//stampa dell'albero 
	printf("\n-----BIBLIOTECA------\n");
	stampaInorder(biblio);

	//variabile per fare pi� operazioni
	int scelta = 0;
	//variabile per confermare l'inserimento degli elementi
	int conf = 0;
	//variabile che salva la matricola in input
	char matricola[100];
	//variabile che salva il titolo del libro in input
	char titolo[100];
	//variabile che salva il tipo di richiesta in input
	int tipo = 0;
	
	do{
		
		printf("\n----------Selezione---------\n");
		printf ("\nPremi\n1-Per aggiungere una richiesta\n2-Per elaborare la richiesta\n0-Per uscire: ");
		scanf("%d",&scelta);
		
	

		switch (scelta){
			//Aggiungere una richiesta di uno studente
			case 1: {
				
				do{
					
					printf("-------------------");
					fgets(matricola,100,stdin);
				
					printf("\nAggiungi matricola: ");
					fgets(matricola,100,stdin);
			
					
					printf("\nAggiungi titolo: ");
					fgets(titolo,100,stdin);
			
					printf("\nAggiungi tipo di richiesta (0 per prendere in prestito, 1 per restituire un prestito): ");
					scanf("%d", &tipo);
					
					printf("\nPremi 1 per confermare: ");
					scanf("%d",&conf);
			
				}while(conf != 1);	
				int esiste = 0;
				//verifica se il libro si trova in biblioteca
				if(libroEsiste(biblio,titolo, esiste)){
					printf("\nRichiesta presa in carico\n");
					queue = init(queue, matricola, tipo, titolo,0);
				}else {
					printf("\nRichiesta non andata a buon fine, libro non contenuto nella biblioteca\n");
				}
				
				break;
			}
			//Prendere in carico una richiesta, ovvero: a. Soddisfare la richiesta oppure b. Sospendere la richiesta (se il libro richiesto è momentaneamente non disponibile) 
			case 2:{
				
				if(!vuoto(queue) || !vuoto(sospesi)){
						
						printf("\n\n------elaborazione-----\n");
					
					while( queue != NULL  ) {
							//ricerca nell'albero
							search(biblio, queue->libro,queue->tipo, queue->matricola,&queue->verify);
							if(queue->verify == 1){
									sospesi = init(sospesi, queue->matricola,queue->tipo,queue->libro,queue->verify);
									queue = canc(queue);
							}
							else {
								//se viene restituito un libro successivamente verifica la coda dei sospesi
								if(queue->tipo == 1) {
										if(!vuoto(sospesi) ){
											studente* tmp_Sos = sospesi;
											while(tmp_Sos !=NULL  ) {
												search(biblio, tmp_Sos->libro,tmp_Sos->tipo, tmp_Sos->matricola,&tmp_Sos->verify);
												if(tmp_Sos->verify == 0) 
													sospesi= cancElem(sospesi,tmp_Sos);
												if(sospesi == NULL) break;
												tmp_Sos = tmp_Sos->next;
											}
										}
								}
								queue = canc(queue);
							}
								
					
					}
				
					if (!vuoto(sospesi)){ 
						
						printf("\n------Biblioteca-----");
						stampaInorder(biblio);
						printf("\n------Coda------");
						stamp(sospesi);
						printf("\n---------------\n");
						printf("\nCi sono richieste in sospeso, Controllare la coda e biblioteca\n"); 
					}
					else{
						stamp(sospesi);
						if(vuoto(sospesi)) printf("\nTutte le richieste sono state soddisfatte\n");
						printf("\n------Finito------\n");
					}
				}
				else {
					printf("\nNon ci sono richieste!");
				}
				break;
			}
			case 0: 
				if(vuoto(sospesi)){
					printf("\nCiao!\n");
					break;
				}else {
					printf("\nNon puoi chiudere Il programma perchè ci sono elementi in coda!");
					printf("\nEffettua un'altra operazione\n\n ");
					printf("\n------Biblioteca-----");
					stampaInorder(biblio);
					printf("\n------Coda------");
					stamp(queue);
					printf("\n---------------\n");
					printf("\nCi sono richieste in sospeso, Controllare la coda e biblioteca\n"); 
					scelta = 3;
				}
				
			case 3: break;
			default: {
				printf ("\nComando non riconosciuto\n");
				break;
			}
		}
	}while(scelta != 0);

	
	
	dealloca(biblio);
  
  	system("pause");
	return 0;
  
}


